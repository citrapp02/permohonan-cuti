package com.citra.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.citra.permohonancuti.models.UserLeaveRequest;

@Repository
public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long>, PagingAndSortingRepository<UserLeaveRequest, Long> {
}
