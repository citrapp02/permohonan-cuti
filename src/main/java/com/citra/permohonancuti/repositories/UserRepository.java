package com.citra.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.citra.permohonancuti.models.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

}
