package com.citra.permohonancuti.service;

import java.util.List;

import com.citra.permohonancuti.models.UserLeaveRequest;

public interface IUserLeaveRequestService {
	List<UserLeaveRequest> findPaginated(int pageNo, int pageSize);
}
