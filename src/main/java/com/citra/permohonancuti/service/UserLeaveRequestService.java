package com.citra.permohonancuti.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.citra.permohonancuti.models.UserLeaveRequest;
import com.citra.permohonancuti.repositories.UserLeaveRequestRepository;

@Service
public class UserLeaveRequestService implements IUserLeaveRequestService {
	@Autowired
	private UserLeaveRequestRepository leaveRequestRepo;

	@Override
	public List<UserLeaveRequest> findPaginated(int pageNo, int pageSize) {
		Pageable paging = PageRequest.of(pageNo, pageSize);
		Page<UserLeaveRequest> pagedResult = leaveRequestRepo.findAll(paging);

		return pagedResult.toList();
	}

}
