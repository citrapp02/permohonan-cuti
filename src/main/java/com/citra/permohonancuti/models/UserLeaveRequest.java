package com.citra.permohonancuti.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "user_leave_requests", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "updateDate" }, allowGetters = true)
public class UserLeaveRequest extends Auditable<String> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1794891886571180043L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_user_leave_request_id_user_request_seq")
	@SequenceGenerator(name = "generator_user_leave_request_id_user_request_seq", sequenceName = "user_leave_request_id_user_request_seq", schema = "public", allocationSize = 1)
	@Column(name = "id_user_leave_request", unique = true, nullable = false)
	private Long idUserLeaveRequest;

	@Column(name = "status", nullable = false)
	private String status;

	@Column(name = "date_submission", nullable = false, updatable = false)
	@Temporal(TemporalType.DATE)
	@CreatedDate
	private Date dateSubmission;

	@Column(name = "leave_date_from", nullable = false)
	private Date leaveDateFrom;

	@Column(name = "leave_date_to", nullable = false)
	private Date leaveDateTo;

	@Column(name = "leave_quota", nullable = false)
	private Integer leaveQuota;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "resolved_reason")
	private String resolvedReason;

	@Column(name = "resolved_by")
	private String resolvedBy;

	@Column(name = "resolved_date")
	private Date resolvedDate;

	@ManyToOne
	@JoinColumn(name = "id_user")
	private Users users;

	@OneToOne(mappedBy = "userLeaveRequest")
	private BucketApproval bucketApproval;

	public UserLeaveRequest() {
		// TODO Auto-generated constructor stub
	}

	public UserLeaveRequest(Long idUserLeaveRequest, String status, Date dateSubmission, Date leaveDateFrom,
			Date leaveDateTo, Integer leaveQuota, String description, String resolvedReason, String resolvedBy,
			Date resolvedDate, Users users, BucketApproval bucketApproval) {
		super();
		this.idUserLeaveRequest = idUserLeaveRequest;
		this.status = status;
		this.dateSubmission = dateSubmission;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.leaveQuota = leaveQuota;
		this.description = description;
		this.resolvedReason = resolvedReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.users = users;
		this.bucketApproval = bucketApproval;
	}

	public Long getIdUserLeaveRequest() {
		return idUserLeaveRequest;
	}

	public void setIdUserLeaveRequest(Long idUserLeaveRequest) {
		this.idUserLeaveRequest = idUserLeaveRequest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateSubmission() {
		return dateSubmission;
	}

	public void setDateSubmission(Date dateSubmission) {
		this.dateSubmission = dateSubmission;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public Integer getLeaveQuota() {
		return leaveQuota;
	}

	public void setLeaveQuota(Integer leaveQuota) {
		this.leaveQuota = leaveQuota;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResolvedReason() {
		return resolvedReason;
	}

	public void setResolvedReason(String resolvedReason) {
		this.resolvedReason = resolvedReason;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public BucketApproval getBucketApproval() {
		return bucketApproval;
	}

	public void setBucketApproval(BucketApproval bucketApproval) {
		this.bucketApproval = bucketApproval;
	}

}
