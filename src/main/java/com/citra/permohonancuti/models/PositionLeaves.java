package com.citra.permohonancuti.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "position_leaves", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdDate", "updateDate" }, allowGetters = true)
public class PositionLeaves extends Auditable<String> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7933878875495387919L;

	@Id
	@Column(name = "id_position_leave", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_position_leave_id_position_leave_seq")
	@SequenceGenerator(name = "generator_position_leave_id_position_leave_seq", sequenceName = "position_leave_id_position_leave_seq", schema = "public", allocationSize = 1)
	private Long idPositionLeave;

	@Column(name = "quota_leave")
	private Integer quotaLeave;

	@OneToOne(mappedBy = "positionLeave")
	private Position position;

	public PositionLeaves() {
		// TODO Auto-generated constructor stub
	}

	public PositionLeaves(Long idPositionLeave, Integer quotaLeave, Position position) {
		super();
		this.idPositionLeave = idPositionLeave;
		this.quotaLeave = quotaLeave;
		this.position = position;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Long getIdPositionLeave() {
		return idPositionLeave;
	}

	public void setIdPositionLeave(Long idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	public Integer getQuotaLeave() {
		return quotaLeave;
	}

	public void setQuotaLeave(Integer quotaLeave) {
		this.quotaLeave = quotaLeave;
	}

}
