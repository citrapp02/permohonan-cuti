package com.citra.permohonancuti.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "users", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdDate", "updateDate" }, allowGetters = true)
public class Users extends Auditable<String> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2064833810376323358L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_user_id_user_seq")
	@SequenceGenerator(name = "generator_user_id_user_seq", sequenceName = "user_id_user_seq", schema = "public", allocationSize = 1)
	@Column(name = "id_user", unique = true, nullable = false)
	private Long idUser;

	@Column(name = "name_user", nullable = false)
	private String name;

	@Column(name = "address", nullable = false)
	private String address;

	@Column(name = "phone", nullable = false)
	private String phone;

	@ManyToOne
	@JoinColumn(name = "id_position")
	private Position position;

	@OneToMany(mappedBy = "users")
	private Set<UserLeaveRequest> userLeaveRequest;

	public Users() {
		// TODO Auto-generated constructor stub
	}

	public Users(Long idUser, String name, String address, String phone, Position position,
			Set<UserLeaveRequest> userLeaveRequest) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.position = position;
		this.userLeaveRequest = userLeaveRequest;
	}

//	public Users(Long idUser, String name, String address, String phone, Position position,
//			Set<UserLeaveRequest> userLeaveRequest, Date createdDate, String createdBy, Date updateDate,
//			String updatedBy) {
//		super();
//		this.idUser = idUser;
//		this.name = name;
//		this.address = address;
//		this.phone = phone;
//		this.position = position;
//		this.userLeaveRequest = userLeaveRequest;
//		this.createdDate = createdDate;
//		this.createdBy = createdBy;
//		this.updateDate = updateDate;
//		this.updatedBy = updatedBy;
//	}

//	public Date getCreatedDate() {
//		return createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public String getCreatedBy() {
//		return createdBy;
//	}
//
//	public void setCreatedBy(String createdBy) {
//		this.createdBy = createdBy;
//	}
//
//	public Date getUpdateDate() {
//		return updateDate;
//	}
//
//	public void setUpdateDate(Date updateDate) {
//		this.updateDate = updateDate;
//	}
//
//	public String getUpdatedBy() {
//		return updatedBy;
//	}
//
//	public void setUpdatedBy(String updatedBy) {
//		this.updatedBy = updatedBy;
//	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Set<UserLeaveRequest> getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public void setUserLeaveRequest(Set<UserLeaveRequest> userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

}
