package com.citra.permohonancuti.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "positions", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdDate", "updateDate" }, allowGetters = true)
public class Position extends Auditable<String> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7145957721921807450L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_position_id_position_seq")
	@SequenceGenerator(name = "generator_position_id_position_seq", sequenceName = "position_id_position_seq", schema = "public", allocationSize = 1)
	@Column(name = "id_position", unique = true, nullable = false)
	private Long idPosition;

	@Column(name = "name_position")
	private String name;

	@OneToOne
	@JoinColumn(name = "id_position_leave")
	private PositionLeaves positionLeave;

	@OneToMany(mappedBy = "position")
	private Set<Users> users;

	public Position() {
		// TODO Auto-generated constructor stub
	}

	public Position(Long idPosition, String name, PositionLeaves positionLeave, Set<Users> users) {
		super();
		this.idPosition = idPosition;
		this.name = name;
		this.positionLeave = positionLeave;
		this.users = users;
	}

	public Set<Users> getUsers() {
		return users;
	}

	public void setUsers(Set<Users> users) {
		this.users = users;
	}

	public Long getIdPosition() {
		return idPosition;
	}

	public void setIdPosition(Long idPosition) {
		this.idPosition = idPosition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PositionLeaves getPositionLeave() {
		return positionLeave;
	}

	public void setPositionLeave(PositionLeaves positionLeave) {
		this.positionLeave = positionLeave;
	}

}
