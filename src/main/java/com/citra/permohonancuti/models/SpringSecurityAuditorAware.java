package com.citra.permohonancuti.models;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import com.citra.permohonancuti.repositories.UserRepository;

public class SpringSecurityAuditorAware implements AuditorAware<String>{
	
	@Autowired
	UserRepository userRepo;
	
	@Override
	public Optional<String> getCurrentAuditor(){
		// return a string representing the username
		return Optional.ofNullable("Citra").filter(s -> !s.isEmpty());
		
	}
}
