package com.citra.permohonancuti.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "bucket_approvals", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdDate", "updateDate" }, allowGetters = true)
public class BucketApproval extends Auditable<String> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5428746314988744301L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_bucket_approval_id_bucket_approval_seq")
	@SequenceGenerator(name = "generator_bucket_approval_id_bucket_approval_seq", sequenceName = "bucket_approval_id_bucket_approval_seq", schema = "public", allocationSize = 1)
	@Column(name = "id_bucket_approval", unique = true, nullable = false)
	private Long idBucketApproval;

	@Column(name = "status_submission", nullable = false)
	private String status;

	@Column(name = "resolved_by")
	private String resolvedBy;

	@Column(name = "resolved_date")
	private Date resolvedDate;

	@Column(name = "resolved_reason")
	private String resolvedReason;

	@OneToOne
	@JoinColumn(name = "id_user_leave_request", updatable = false)
	private UserLeaveRequest userLeaveRequest;

	public BucketApproval() {
		// TODO Auto-generated constructor stub
	}

	public BucketApproval(Long idBucketApproval, String status, String resolvedBy,
			Date resolvedDate, String resolvedReason, UserLeaveRequest userLeaveRequest) {
		super();
		this.idBucketApproval = idBucketApproval;
		this.status = status;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.resolvedReason = resolvedReason;
		this.userLeaveRequest = userLeaveRequest;
	}

	public Long getIdBucketApproval() {
		return idBucketApproval;
	}

	public void setIdBucketApproval(Long idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getResolvedReason() {
		return resolvedReason;
	}

	public void setResolvedReason(String resolvedReason) {
		this.resolvedReason = resolvedReason;
	}

	public UserLeaveRequest getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public void setUserLeaveRequest(UserLeaveRequest userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

}
