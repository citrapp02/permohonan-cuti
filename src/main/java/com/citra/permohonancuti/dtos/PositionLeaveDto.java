package com.citra.permohonancuti.dtos;

public class PositionLeaveDto {
	private Long idPositionLeave;
	private Integer quotaLeave;

	public PositionLeaveDto() {
		// TODO Auto-generated constructor stub
	}

	public PositionLeaveDto(Long idPositionLeave, Integer quotaLeave) {
		super();
		this.idPositionLeave = idPositionLeave;
		this.quotaLeave = quotaLeave;
	}

	public Long getIdPositionLeave() {
		return idPositionLeave;
	}

	public void setIdPositionLeave(Long idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	public Integer getQuotaLeave() {
		return quotaLeave;
	}

	public void setQuotaLeave(Integer quotaLeave) {
		this.quotaLeave = quotaLeave;
	}

}
