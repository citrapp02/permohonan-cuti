package com.citra.permohonancuti.dtos;

public class UserDto {
	private Long idUser;
	private String name;
	private String address;
	private String phone;
	private PositionDto position;

	public UserDto() {
		// TODO Auto-generated constructor stub
	}

	public UserDto(Long idUser, String name, String address, String phone, PositionDto position) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.position = position;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public PositionDto getPosition() {
		return position;
	}

	public void setPosition(PositionDto position) {
		this.position = position;
	}

}
