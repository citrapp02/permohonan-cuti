package com.citra.permohonancuti.dtos;

import java.util.Date;

public class BucketApprovalDto {
	private Long idBucketApproval;
	private String status;
	private Date dateSubmission;
	private String resolvedBy;
	private Date resolvedDate;
	private String resolvedReason;
	private LeaveRequestDto userLeaveRequest;

	public BucketApprovalDto() {
		// TODO Auto-generated constructor stub
	}

	public BucketApprovalDto(Long idBucketApproval, String status, Date dateSubmission, String resolvedBy,
			Date resolvedDate, String resolvedReason, LeaveRequestDto userLeaveRequest) {
		super();
		this.idBucketApproval = idBucketApproval;
		this.status = status;
		this.dateSubmission = dateSubmission;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.resolvedReason = resolvedReason;
		this.userLeaveRequest = userLeaveRequest;
	}

	public Long getIdBucketApproval() {
		return idBucketApproval;
	}

	public void setIdBucketApproval(Long idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateSubmission() {
		return dateSubmission;
	}

	public void setDateSubmission(Date dateSubmission) {
		this.dateSubmission = dateSubmission;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getResolvedReason() {
		return resolvedReason;
	}

	public void setResolvedReason(String resolvedReason) {
		this.resolvedReason = resolvedReason;
	}

	public LeaveRequestDto getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public void setUserLeaveRequest(LeaveRequestDto userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

}
