package com.citra.permohonancuti.dtos;

public class PositionDto {
	private Long idPosition;
	private String name;
	private PositionLeaveDto positionLeave;

	public PositionDto() {
		// TODO Auto-generated constructor stub
	}

	public PositionDto(Long idPosition, String name, PositionLeaveDto positionLeave) {
		super();
		this.idPosition = idPosition;
		this.name = name;
		this.positionLeave = positionLeave;
	}

	public PositionLeaveDto getPositionLeave() {
		return positionLeave;
	}

	public void setPositionLeave(PositionLeaveDto positionLeave) {
		this.positionLeave = positionLeave;
	}

	public Long getIdPosition() {
		return idPosition;
	}

	public void setIdPosition(Long idPosition) {
		this.idPosition = idPosition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
