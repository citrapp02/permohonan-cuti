package com.citra.permohonancuti.dtos;

import java.util.Date;

public class LeaveRequestDto {
	private Long idUserLeaveRequest;
	private String status;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private Integer leaveQuota;
	private String description;
	private String resolvedReason;
	private String resolvedBy;
	private Date resolvedDate;
	private Date dateSubmission;
	private UserDto users;

	public LeaveRequestDto() {
		// TODO Auto-generated constructor stub
	}

	public LeaveRequestDto(Long idUserLeaveRequest, String status, Date leaveDateFrom, Date leaveDateTo,
			Integer leaveQuota, String description, String resolvedReason, String resolvedBy, Date resolvedDate,
			Date dateSubmission, UserDto users) {
		super();
		this.idUserLeaveRequest = idUserLeaveRequest;
		this.status = status;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.leaveQuota = leaveQuota;
		this.description = description;
		this.resolvedReason = resolvedReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.dateSubmission = dateSubmission;
		this.users = users;
	}

	public Date getDateSubmission() {
		return dateSubmission;
	}

	public void setDateSubmission(Date dateSubmission) {
		this.dateSubmission = dateSubmission;
	}

	public Long getIdUserLeaveRequest() {
		return idUserLeaveRequest;
	}

	public void setIdUserLeaveRequest(Long idUserLeaveRequest) {
		this.idUserLeaveRequest = idUserLeaveRequest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public Integer getLeaveQuota() {
		return leaveQuota;
	}

	public void setLeaveQuota(Integer leaveQuota) {
		this.leaveQuota = leaveQuota;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResolvedReason() {
		return resolvedReason;
	}

	public void setResolvedReason(String resolvedReason) {
		this.resolvedReason = resolvedReason;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public UserDto getUsers() {
		return users;
	}

	public void setUsers(UserDto users) {
		this.users = users;
	}

}
