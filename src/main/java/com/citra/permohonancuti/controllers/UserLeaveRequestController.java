package com.citra.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.citra.permohonancuti.dtos.LeaveRequestDto;
import com.citra.permohonancuti.models.BucketApproval;
import com.citra.permohonancuti.models.UserLeaveRequest;
import com.citra.permohonancuti.models.Users;
import com.citra.permohonancuti.repositories.BucketApprovalRepository;
import com.citra.permohonancuti.repositories.UserLeaveRequestRepository;
import com.citra.permohonancuti.repositories.UserRepository;
import com.citra.permohonancuti.service.IUserLeaveRequestService;

@RestController
@RequestMapping("/permohonan-cuti/leave-request")
public class UserLeaveRequestController {
	@Autowired
	UserLeaveRequestRepository leaveRequestRepo;

	@Autowired
	UserRepository userRepo;

	@Autowired
	BucketApprovalRepository approvalRepo;

	@Autowired
	private IUserLeaveRequestService requestService;

	ModelMapper modelMapper = new ModelMapper();

	@PostMapping
	public Map<String, Object> requestLeave(@Valid @RequestBody LeaveRequestDto body) {
		Map<String, Object> result = new HashMap<String, Object>();

		UserLeaveRequest requestEntity = new UserLeaveRequest();

		requestEntity = modelMapper.map(body, UserLeaveRequest.class);

		// ==== set leave quota ======
		List<UserLeaveRequest> leaveRequestList = leaveRequestRepo.findAll();

		Users user = getUserId(body); // get the user that requesting leave

		// to store leaveRequest data that already have the same user
		List<UserLeaveRequest> userExist = getExistUser(leaveRequestList, user);

		Integer currentQuota = getInitialQuota(user); // initial quota

		currentQuota = getCurrentQuota(leaveRequestList, userExist, currentQuota);

		Long leaveFrom = body.getLeaveDateFrom().getTime();

		double leaveDays = getLeaveDays(body, leaveFrom);

		setQuotaStatusSaveAndMessage(result, requestEntity, currentQuota, leaveDays); // also set for status, save and
																						// messages

		// ==== End of set leave quota ======

		body.setIdUserLeaveRequest(requestEntity.getIdUserLeaveRequest());

		result.put("Data", body);

		return result;
	}

	@GetMapping("/leaveRequest/{pageNo}/{pageSize}")
	public Map<String, Object> getPaginatedUserLeaveRequest(@PathVariable int pageNo, @PathVariable int pageSize) {
		Map<String, Object> result = new HashMap<String, Object>();

		List<UserLeaveRequest> listRequest = requestService.findPaginated(pageNo, pageSize);

		List<LeaveRequestDto> listRequestDto = new ArrayList<LeaveRequestDto>();

		for (UserLeaveRequest request : listRequest) {
			LeaveRequestDto requestDto = new LeaveRequestDto();
			requestDto = modelMapper.map(request, LeaveRequestDto.class);

			listRequestDto.add(requestDto);
		}

		result.put("Message", "Read Success");
		result.put("Data", listRequestDto);

		return result;
	}

	public void setQuotaStatusSaveAndMessage(Map<String, Object> result, UserLeaveRequest requestEntity,
			Integer currentQuota, double leaveDays) {
		if (leaveDays >= 0) {
			if (currentQuota >= leaveDays) {
				result.put("Message", "Your requests in proccess..");
				requestEntity.setLeaveQuota((int) (currentQuota));
				requestEntity.setStatus("Waiting..."); // set initial request status
				leaveRequestRepo.save(requestEntity);

				BucketApproval approvalEntity = modelMapper.map(requestEntity, BucketApproval.class);
				approvalRepo.save(approvalEntity);
			} else if (currentQuota == 0) {
				result.put("Message", "Sorry, you run out of leave days..");

			} else if (currentQuota < 0) {
				result.put("Message", "Sorry, you only have " + currentQuota + " leave days quota left.");
			} else {
				result.put("Message", "The date you are submit allready passed."); // condition for backDate
			}
		} else {
			result.put("Message", "Not valid requested leave date");
		}
	}

	public Integer getCurrentQuota(List<UserLeaveRequest> leaveRequestList, List<UserLeaveRequest> userExist,
			Integer currentQuota) {
		if (userExist.isEmpty()) { // for initial leave quota, if the user request leave for the first time

		} else {
			for (UserLeaveRequest leaveRequest : leaveRequestList) {
				currentQuota = leaveRequest.getLeaveQuota();
			}
		}
		return currentQuota;
	}

	public double getLeaveDays(LeaveRequestDto body, Long leaveFrom) {

		Long leaveTo = body.getLeaveDateTo().getTime();
		double leaveDays = TimeUnit.MILLISECONDS.toDays(leaveTo - leaveFrom);

		return leaveDays;
	}

	public boolean getBackDateValidation(LeaveRequestDto body, Long leaveFrom) {
		Long currentDate = body.getDateSubmission().getTime();
		double backDate = TimeUnit.MILLISECONDS.toDays(currentDate - leaveFrom);
		boolean backDatIsValid = backDate > 0;

		return backDatIsValid;
	}

	public List<UserLeaveRequest> getExistUser(List<UserLeaveRequest> leaveRequestList, Users user) {
		List<UserLeaveRequest> userExist = new ArrayList<UserLeaveRequest>();

		for (UserLeaveRequest leaveRequest : leaveRequestList) {
			if (leaveRequest.getUsers() == user) {
				userExist.add(leaveRequest);
			}
		}
		return userExist;
	}

	// get user id
	public Users getUserId(LeaveRequestDto body) {
		Long idUser = body.getUsers().getIdUser();

		Users users = userRepo.findById(idUser).get();
		return users;
	}

	// set initial quota leave for user
	public Integer getInitialQuota(Users users) {

		return users.getPosition().getPositionLeave().getQuotaLeave();

	}

}
