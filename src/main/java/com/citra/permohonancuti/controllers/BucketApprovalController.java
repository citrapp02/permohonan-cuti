package com.citra.permohonancuti.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.citra.permohonancuti.dtos.BucketApprovalDto;
import com.citra.permohonancuti.exception.ResourceNotFoundException;
import com.citra.permohonancuti.models.BucketApproval;
import com.citra.permohonancuti.models.UserLeaveRequest;
import com.citra.permohonancuti.repositories.BucketApprovalRepository;
import com.citra.permohonancuti.repositories.UserLeaveRequestRepository;

@RestController
@RequestMapping("/permohonan-cuti/bucket-approval")
public class BucketApprovalController {
	@Autowired
	BucketApprovalRepository approvalRepo;

	@Autowired
	UserLeaveRequestRepository requestRepo;

	ModelMapper modelMapper = new ModelMapper();

	@PutMapping("/approval")
	public Map<String, Object> resolveRequestLeave(@Valid @RequestBody BucketApprovalDto body,
			@RequestParam(name = "approvalId") Long approvalId,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
		Map<String, Object> result = new HashMap<String, Object>();

		BucketApproval approvalEntity = approvalRepo.findById(approvalId)
				.orElseThrow(() -> new ResourceNotFoundException("Bucket Approval", "approvalId", approvalId));

		Long idRequest = getRequestLeaveId(approvalEntity);

		UserLeaveRequest requestEntity = getUserLeaveRequest(idRequest);

		setRequestEntity(body, date, requestEntity);

		approvalEntity = modelMapper.map(body, BucketApproval.class);
		approvalEntity.setResolvedDate(date);

		approvalEntity.setIdBucketApproval(approvalId);

		boolean resolveEmployeeIsValid = getEmployeeValidation(body, requestEntity);
		boolean resolveSupervisorIsValid = getSupervisorValidation(body, requestEntity);
		boolean resolveStaffIsValid = getStaffValidation(body, requestEntity);

		boolean positionIsValid = resolveEmployeeIsValid || resolveStaffIsValid || resolveSupervisorIsValid;

		if (positionIsValid) {
			if (date.compareTo(requestEntity.getLeaveDateFrom()) < 0) {
				result.put("Message", "Wrong input, the resolvedDate should not earlier than leaveDateFrom!");
			} else if (date.compareTo(requestEntity.getLeaveDateFrom()) >= 0) {
				approvalRepo.save(approvalEntity);
				approvalEntity.setIdBucketApproval(body.getIdBucketApproval());
				result.put("Message",
						"Leave Request with ID " + requestEntity.getIdUserLeaveRequest() + " was decided.");
			}
		} else {
			result.put("Message", "Resolver is not valid");
		}

		return result;
	}

	@GetMapping("/getApproval")
	public Map<String, Object> getBucketApprovaById(@RequestParam(name = "approvalId") Long approvalId) {
		Map<String, Object> result = new HashMap<String, Object>();
		BucketApproval approvalData = approvalRepo.findById(approvalId)
				.orElseThrow(() -> new ResourceNotFoundException("Bucket Approval", "approvalId", approvalId));

		BucketApprovalDto approvalDto = new BucketApprovalDto();
		approvalDto = modelMapper.map(approvalData, BucketApprovalDto.class);

		result.put("Message", "Get Bcuket Approval By Id Success");
		result.put("Data", approvalDto);

		return result;
	}

	public boolean getStaffValidation(BucketApprovalDto body, UserLeaveRequest requestEntity) {
		return body.getResolvedBy().contains("Staff")
				&& requestEntity.getUsers().getPosition().getName().contains("Staff");
	}

	public boolean getSupervisorValidation(BucketApprovalDto body, UserLeaveRequest requestEntity) {
		return body.getResolvedBy().contains("Supervisor")
				&& requestEntity.getUsers().getPosition().getName().contains("Supervisor");
	}

	public boolean getEmployeeValidation(BucketApprovalDto body, UserLeaveRequest requestEntity) {
		return body.getResolvedBy().contains("Supervisor")
				&& requestEntity.getUsers().getPosition().getName().contains("Employee");
	}

	public void setRequestEntity(BucketApprovalDto body, Date date, UserLeaveRequest requestEntity) {
		requestEntity.setStatus(body.getStatus());
		requestEntity.setResolvedBy(body.getResolvedBy());
		requestEntity.setResolvedReason(body.getResolvedReason());
		requestEntity.setResolvedDate(date);

		// set quota for leaveRequest after resolve status is approve
		Integer currentQuota = requestEntity.getLeaveQuota();

		double leaveDays = getLeaveDays(requestEntity);

		if (body.getStatus().toLowerCase().contains("approve")) {
			requestEntity.setLeaveQuota((int) (currentQuota - leaveDays));
		}
	}

	public double getLeaveDays(UserLeaveRequest requestEntity) {
		Long leaveFrom = requestEntity.getLeaveDateFrom().getTime();
		Long leaveTo = requestEntity.getLeaveDateTo().getTime();
		double leaveDays = TimeUnit.MILLISECONDS.toDays(leaveTo - leaveFrom);
		return leaveDays;
	}

	public UserLeaveRequest getUserLeaveRequest(Long idRequest) {
		return requestRepo.findById(idRequest)
				.orElseThrow(() -> new ResourceNotFoundException("User Leave Request", "requestId", idRequest));
	}

	public Long getRequestLeaveId(BucketApproval approvalEntity) {
		return approvalEntity.getUserLeaveRequest().getIdUserLeaveRequest();
	}

}
