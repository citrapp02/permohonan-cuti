package com.citra.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.citra.permohonancuti.dtos.UserDto;
import com.citra.permohonancuti.exception.ResourceNotFoundException;
import com.citra.permohonancuti.models.Users;
import com.citra.permohonancuti.repositories.UserRepository;

@RestController
@RequestMapping("/permohonan-cuti/users")
public class UserController {
	@Autowired
	UserRepository userRepo;

	ModelMapper modelMapper = new ModelMapper();

	// create new users
	@PostMapping("/create")
	public Map<String, Object> createUser(@Valid @RequestBody UserDto body) {
		Map<String, Object> result = new HashMap<String, Object>();

		Users userEntity = new Users();
		userEntity = modelMapper.map(body, Users.class);

		userRepo.save(userEntity);

		body.setIdUser(userEntity.getIdUser());

		result.put("Message", "Create a new user leave");
		result.put("Data", body);

		return result;
	}

	// Get All
	@GetMapping("/getAll")
	public Map<String, Object> getAllUser() {
		Map<String, Object> result = new HashMap<String, Object>();

		List<Users> listAllUser = userRepo.findAll();

		List<UserDto> listUserDto = new ArrayList<UserDto>();

		for (Users userEntity : listAllUser) {
			UserDto userDto = new UserDto();
			userDto = modelMapper.map(userEntity, UserDto.class);

			listUserDto.add(userDto);
		}

		result.put("Message", "Read All Success");
		result.put("Data", listUserDto);
		result.put("Total Data", listUserDto.size());

		return result;
	}

	// get by id
	@GetMapping("/getById")
	public Map<String, Object> getUserById(@RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Users userData = userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

		UserDto userDto = new UserDto();
		userDto = modelMapper.map(userData, UserDto.class);

		result.put("Message", "Get an User By Id Success");
		result.put("Data", userDto);

		return result;
	}

	// album update
	@PutMapping("/update")
	public Map<String, Object> updateUser(@Valid @RequestBody UserDto body, @RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();

		Users userEntity = userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

		userEntity = modelMapper.map(body, Users.class);
		userEntity.setIdUser(id);

		userRepo.save(userEntity);

		body.setIdUser(userEntity.getIdUser());

		result.put("Message", "User Updated.");
		result.put("Data", body);

		return result;
	}

	// delete
	@DeleteMapping("/delete")
	public Map<String, Object> deleteUser(@RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();

		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

		UserDto userDto = new UserDto();
		userDto = modelMapper.map(userEntity, UserDto.class);

		userRepo.deleteById(id);

		result.put("Message", "Delete an User Success");
		result.put("Deeted data", userDto);

		return result;
	}

}
