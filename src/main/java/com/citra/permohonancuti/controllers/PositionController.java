package com.citra.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.citra.permohonancuti.dtos.PositionDto;
import com.citra.permohonancuti.exception.ResourceNotFoundException;
import com.citra.permohonancuti.models.Position;
import com.citra.permohonancuti.repositories.PositionRepository;

@RestController
@RequestMapping("/permohonan-cuti/position")
public class PositionController {
	@Autowired
	PositionRepository positionRepo;

	ModelMapper modelMapper = new ModelMapper();

	// create new position
	@PostMapping("/create")
	public Map<String, Object> createPosition(@Valid @RequestBody PositionDto body) {
		Map<String, Object> result = new HashMap<String, Object>();

		Position positionEntity = new Position();

		positionEntity = modelMapper.map(body, Position.class);

		positionRepo.save(positionEntity);

		body.setIdPosition(positionEntity.getIdPosition());

		result.put("Message", "Create new position ");
		result.put("Data", body);

		return result;
	}

	// Get All
	@GetMapping("/getAll")
	public Map<String, Object> getAllPosition() {
		Map<String, Object> result = new HashMap<String, Object>();

		List<Position> listAllPosition = positionRepo.findAll();

		List<PositionDto> listPositionDto = new ArrayList<PositionDto>();

		for (Position positionEntity : listAllPosition) {
			PositionDto positionDto = new PositionDto();
			positionDto = modelMapper.map(positionEntity, PositionDto.class);

			listPositionDto.add(positionDto);
		}

		result.put("Message", "Read All Success");
		result.put("Data", listPositionDto);
		result.put("Total Data", listPositionDto.size());

		return result;
	}

	// get by id
	@GetMapping("/getById")
	public Map<String, Object> getPositionById(@RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Position positionData = positionRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position", "id", id));

		PositionDto positionDto = new PositionDto();
		positionDto = modelMapper.map(positionData, PositionDto.class);

		result.put("Message", "Get Position By Id Success");
		result.put("Data", positionDto);

		return result;
	}

	// album update
	@PutMapping("/update")
	public Map<String, Object> updatePosition(@Valid @RequestBody PositionDto body,
			@RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();

		Position positionEntity = positionRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position", "id", id));

		positionEntity = modelMapper.map(body, Position.class);
		positionEntity.setIdPosition(id);

		positionRepo.save(positionEntity);

		body.setIdPosition(positionEntity.getIdPosition());

		result.put("Message", "Position Updated.");
		result.put("Data", body);

		return result;
	}

	// delete
	@DeleteMapping("/delete")
	public Map<String, Object> deletePosition(@RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();

		Position positionEntity = positionRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position", "id", id));

		PositionDto positionDto = new PositionDto();
		positionDto = modelMapper.map(positionEntity, PositionDto.class);

		positionRepo.deleteById(id);

		result.put("Message", "Delete a Position Success");
		result.put("Deeted data", positionDto);

		return result;
	}

}
