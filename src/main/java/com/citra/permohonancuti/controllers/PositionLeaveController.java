package com.citra.permohonancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.citra.permohonancuti.dtos.PositionLeaveDto;
import com.citra.permohonancuti.exception.ResourceNotFoundException;
import com.citra.permohonancuti.models.PositionLeaves;
import com.citra.permohonancuti.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("/permohonan-cuti/position-leave")
public class PositionLeaveController {
	@Autowired
	PositionLeaveRepository positionLeaveRepo;

	ModelMapper modelMapper = new ModelMapper();

	// create new position leave
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@Valid @RequestBody PositionLeaveDto body) {
		Map<String, Object> result = new HashMap<String, Object>();

		PositionLeaves positionLeaveEntity = new PositionLeaves();

		positionLeaveEntity = modelMapper.map(body, PositionLeaves.class);

		positionLeaveRepo.save(positionLeaveEntity);

		body.setIdPositionLeave(positionLeaveEntity.getIdPositionLeave());

		result.put("Message", "Create new position leave");
		result.put("Data", body);

		return result;
	}

	// Get All
	@GetMapping("/getAll")
	public Map<String, Object> getAllPositionLeave() {
		Map<String, Object> result = new HashMap<String, Object>();

		List<PositionLeaves> listAllPositionLeave = positionLeaveRepo.findAll();

		List<PositionLeaveDto> listPosLeaveDto = new ArrayList<PositionLeaveDto>();

		for (PositionLeaves posLeaveEntity : listAllPositionLeave) {
			PositionLeaveDto posLeaveDto = new PositionLeaveDto();
			posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeaveDto.class);

			listPosLeaveDto.add(posLeaveDto);
		}

		result.put("Message", "Read All Success");
		result.put("Data", listPosLeaveDto);
		result.put("Total Data", listPosLeaveDto.size());

		return result;
	}

	// get by id
	@GetMapping("/getById")
	public Map<String, Object> getPositionLeaveById(@RequestParam(name = "posLeaveId") Long posLeaveId) {
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeaves posLeaveData = positionLeaveRepo.findById(posLeaveId)
				.orElseThrow(() -> new ResourceNotFoundException("Position Leave", "posLeaveId", posLeaveId));

		PositionLeaveDto posLeaveDto = new PositionLeaveDto();
		posLeaveDto = modelMapper.map(posLeaveData, PositionLeaveDto.class);

		result.put("Message", "Get Position Leave By Id Success");
		result.put("Data", posLeaveDto);

		return result;
	}

	// album update
	@PutMapping("/update")
	public Map<String, Object> updatePositionLeave(@Valid @RequestBody PositionLeaveDto body,
			@RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();

		PositionLeaves posLeaveEntity = positionLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position Leaves", "id", id));

		posLeaveEntity = modelMapper.map(body, PositionLeaves.class);
		posLeaveEntity.setIdPositionLeave(id);

		positionLeaveRepo.save(posLeaveEntity);

		body.setIdPositionLeave(posLeaveEntity.getIdPositionLeave());

		result.put("Message", "Position Leave Updated.");
		result.put("Data", body);

		return result;
	}

	// delete
	@DeleteMapping("/delete")
	public Map<String, Object> deletePositionLeave(@RequestParam(name = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();

		PositionLeaves posLeaveEntity = positionLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position Leave", "id", id));

		PositionLeaveDto posLeaveDto = new PositionLeaveDto();
		posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeaveDto.class);

		positionLeaveRepo.deleteById(id);

		result.put("Message", "Delete a Position Leave Success");
		result.put("Deeted data", posLeaveDto);

		return result;
	}

}
